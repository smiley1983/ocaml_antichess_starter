open Td;;

let height = 8;;
let width = 8;;

(* read height + 1 lines from stdin, return the list of strings *)
let read_lines () =
  let line_count = ref 0 in
  let rec read_loop acc =
    let line = read_line () in
    line_count := !line_count + 1;
    if !line_count = height + 1 then
     (
      List.rev (line :: acc)
     )
    else
      read_loop (line :: acc)
  in
  try Some (read_loop []) with End_of_file -> None
;;

(* tokenizer from rosetta code *)
let split_char sep str =
  let string_index_from i =
    try Some (String.index_from str i sep)
    with Not_found -> None
  in
  let rec aux i acc = match string_index_from i with
    | Some i' ->
        let w = String.sub str i (i' - i) in
        aux (succ i') (w::acc)
    | None ->
        let w = String.sub str i (String.length str - i) in
        List.rev (w::acc)
  in
  aux 0 []
;;

(* messy stateful tracking of row count during map parsing *)
let next_grid_row state =
  let result = state.grid.(state.row_count) in
    state.row_count <- state.row_count + 1;
    result
;;

let parse_map_char = function
  | 'r' -> `Rook, 1
  | 'n' -> `Knight, 1
  | 'b' -> `Bishop, 1
  | 'q' -> `Queen, 1
  | 'k' -> `King, 1
  | 'p' -> `Pawn, 1
  | 'R' -> `Rook, 2
  | 'N' -> `Knight, 2
  | 'B' -> `Bishop, 2
  | 'Q' -> `Queen, 2
  | 'K' -> `King, 2
  | 'P' -> `Pawn, 2
  | _ -> `Empty, 0
;;

(* parse a line of map data into the grid *)
let add_map_line state l =
  let grid_row = next_grid_row state in
  let count = ref 0 in
  let add_tile c = 
    let tile, owner = parse_map_char c in
(*
      if c = state.id then `Friend
      else if c = '-' then `Empty
      else `Enemy
    in
*)
      grid_row.(!count) <- (tile, owner);
      count := !count + 1
  in
    String.iter add_tile l
;;

(* parse an input string consisting of a single token *)
let one_token state t =
  match String.length t with
  | 1 -> state.id <- int_of_string t
  | 8 -> add_map_line state t (* 8 = width FIXME *)
  | _ -> ()
;;

(* parse a line of input *)
let parse_line state l =
  let tokens = split_char ' ' l in
  match List.length tokens with
  | 1 -> one_token state (List.nth tokens 0)
  | _ -> ()
;;

(* If read_lines in update returns None, it means that either the engine did 
 * not send as much info as was expected (not likely unless the format changes)
 * or that this program has processed the game state, submitted its orders,
 * and then returned for more input (more likely). Since hackerrank bots are
 * run afresh every turn and should exit cleanly, "exit 0" is the correct
 * response to None here.
 *)
let update state =
  match read_lines () with
  | None -> exit 0
  | Some ll ->
      List.iter (fun l -> Debug.debug (l ^ "\n"); parse_line state l) ll
;;

let issue_order ((srow, scol), (trow, tcol)) =
  Printf.printf "%d %d %d %d\n" srow scol trow tcol
;;
