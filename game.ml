open Td;;

let in_bounds state (row, col) =
  row >= 0
  && col >= 0
  && row < state.height
  && col < state.width
;;

let cardinal8_offset = [|
    (0, 1);
    (1, 1);
    (1, 0);
    (1, -1);
    (0, -1);
    (-1, -1);
    (-1, 0);
    (-1, 1);
  |]
;;

let cardinal4_offset = [|
    (0, 1);
    (1, 0);
    (0, -1);
    (-1, 0);
  |]
;;

let diagonal_offset = [|
    (1, 1);
    (1, -1);
    (-1, -1);
    (-1, 1);
  |]
;;

let knight_offset = 
  [|
    (-2, 1);
    (-1, 2);
    (1, 2);
    (2, 1);
    (2, -1);
    (1, -2);
    (-1, -2);
    (-2, -1);
  |]
;;

let pawn1_attack =
  [|
    (1, 1);
    (1, -1);
  |]
;;

let pawn2_attack =
  [|
    (-1, 1);
    (-1, -1);
  |]
;;

let pawn1_advance =
  [|
    (1, 0);
  |]
;;

let pawn2_advance =
  [|
    (-1, 0);
  |]
;;

let empty (tile, owner) =
  tile = `Empty
;;

let enemy state (tile, owner) =
  not ((tile = `Empty) || (owner = state.id))
;;

let one_step state (srow, scol) (care_attack, should_attack) =
  let result = ref [] in
  let step' (row, col) (off_row, off_col) =
    if (off_row, off_col) = (-16, -16) then !result
    else
      let trow, tcol = row + off_row, col + off_col in
        if in_bounds state (trow, tcol) then
          let tile = state.grid.(trow).(tcol) in
            if (empty tile) && ((not care_attack) || (not should_attack)) then
             (
              result := (((srow, scol), (trow, tcol)), false) :: !result; 
              []
             )
            else if (enemy state tile) 
              && ((not care_attack) || (should_attack)) 
            then
             (
              result := (((srow, scol), (trow, tcol)), true) :: !result; 
              raise (Failure "encountered enemy piece")
             )
            else raise (Failure "encountered friendly piece")
        else raise (Failure "out of bounds")
  in
    step'
;;

let add_loc (l1r, l1c) (l2r, l2c) = (l1r + l2r), (l1c + l2c);;

let rec iter_step step loc offset = function
  | 0 -> ()
  | limit -> 
      try
       (
        ignore (step loc offset);
        iter_step step (add_loc loc offset) offset (limit - 1)
       )
      with Failure n -> ()
;;

let simple_move state loc offset =
  let step = (one_step state loc (false, false)) in
    Array.iter (fun o -> try ignore (step loc o) with _ -> ()) offset;
    step (0, 0) (-16, -16) (* ask for the return value from the closure *)
;;

let ray_move state loc offset =
  let step = (one_step state loc (false, false)) in
    Array.iter (fun o -> ignore (iter_step step loc o 8)) offset;
    step (0, 0) (-16, -16) (* ask for the return value from the closure *)
;;

let king_moves state loc =
  simple_move state loc cardinal8_offset
;;

let knight_moves state loc =
  simple_move state loc knight_offset
;;

let restricted_move state loc offset should_attack =
  let step = (one_step state loc (true, should_attack) loc) in
    Array.iter (fun o -> try ignore (step o) with _ -> ()) offset;
    step (-16, -16) (* ask for the return value from the closure *)
;;

let pawn_moves state loc owner =
  let pawn_attack = if owner = 1 then pawn1_attack else pawn2_attack in
  let pawn_advance = if owner = 1 then pawn1_advance else pawn2_advance in
    restricted_move state loc pawn_attack true 
    @ restricted_move state loc pawn_advance false
;;

let bishop_moves state loc =
  ray_move state loc diagonal_offset
;;

let rook_moves state loc =
  ray_move state loc cardinal4_offset
;;

let queen_moves state loc =
  ray_move state loc cardinal8_offset
;;

let piece_options state tile loc owner =
  match tile with
  | `Pawn -> pawn_moves state loc owner
  | `Bishop -> bishop_moves state loc
  | `Rook -> rook_moves state loc
  | `Queen -> queen_moves state loc
  | `King -> king_moves state loc
  | `Knight -> knight_moves state loc
  | `Empty -> []
;;

(* make a list of valid moves for a given player *)
let all_moves state player =
  let result = ref [] in
    Array.iteri (fun ir row -> Array.iteri (fun ic (tile, owner) ->
      if (not (tile = `Empty)) && (owner = player) then 
       (
        result := (piece_options state tile (ir, ic) owner) @ !result
       )
    ) row) state.grid;
  !result
;;

let valid_moves state player =
  let m = all_moves state player in
  let attack = List.filter (fun (m, atck) -> atck) m in
    if attack = [] then m else attack
;;

