This is a starter package for hackerrank's Antichess challenge.

It can be packaged into one file using the included one_file program.

To build everything:

ocamlbuild antichess.native 	# check that the source works

ocamlbuild one_file.native	# build the one_file program

./one_file.native		# invoke it

ocamlbuild output.native	# check that you can build the result

The output.ml file should compile correctly when submitted to hackerrank.

