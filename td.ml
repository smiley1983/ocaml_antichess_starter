type tile = [ `Rook | `Knight | `Bishop | `Queen | `King | `Pawn | `Empty ]

(*type dir = [ `N | `E | `S | `W | `NE | `SE | `NW | `SW ];;*)

type state =
 {
  grid : (tile * int) array array;
  mutable height : int;
  mutable width : int;
  mutable row_count : int;
  mutable id : int;
 }
;;

