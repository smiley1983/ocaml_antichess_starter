open Td;;

let new_state () =
 {
  grid = Array.make_matrix Io.height Io.width (`Empty, 0);
  row_count = 0;
  id = -1;
  height = 8;
  width = 8;
 }
;;

(* main input -> engine -> output loop (loop part not needed for hackerrank) *)
let loop engine =
  let rec take_turn i state =
    Io.update state;
    begin try
     (
      engine state;
      flush stdout;
     )
    with exc ->
     (
      Debug.debug (Printf.sprintf
         "Exception in turn %d :\n" i);
      Debug.debug (Printexc.to_string exc);
      raise exc
     )
    end;
    take_turn (i + 1) state
  in
    (* Random.self_init () *) (* seed RNG if you want *)
    let proto_state = new_state () in
    take_turn 0 proto_state
;;

loop Ai.engine
